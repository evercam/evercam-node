![Evercam-node](https://raw.githubusercontent.com/evercam/evercam-node/master/logo.png)

# evercam-node
__An improved Node.js wrapper for the Evercam API.__
Proof we can have nice things

[![Build Status](https://travis-ci.org/evercam/evercam-node.svg)](https://travis-ci.org/evercam/evercam-node)

### Supported Methods

* `evercam = new Evercam(api_id, api_key)` - Create a new Evercam object
* `evercam.url(url)` - Set the API url to something else
* `evercam.test(callback)` - Test the auth details using the /test.json API call

#### Public

* `evercam.public.cameras([options], callback)` - Fetch a list of publicly discoverable cameras

#### Camera

* `evercam.camera.test(external_url, jpg_url, [username], [password], callback)` - Tests if given camera parameters are correct
* `evercam.camera.get(ids_array, callback)` - Returns all data for a specified set of cameras
* `evercam.camera.update(id, changes, callback)` - Updates full or partial data for an existing camera
* `evercam.camera.remove(id, callback)` - Removes a camera from Evercam along with any stored media
* `evercam.camera.transfer(id, userid)` - Transfers the ownership of a camera from one user to another
* `evercam.camera.create(options, callback)` - Creates a new camera owned by the authenticating user

#### Snapshots

* `evercam.snapshots.live(id, callback)` - Returns jpg buffer from a camera in an object
* `evercam.snapshots.last(id, [with_data], callback)` - Returns last snapshot data, `with_data` is true by default
* `evercam.snapshots.save(id, [note], callback)` - Save the current camera snapshot, at the current timestamp
* `evercam.snapshots.store(id, timestamp, data_buffer, filename, [note], callback)` - Upload an image, at a timestamp
* `evercam.snapshots.remove(id, timestamp, callback)` - removes a snapshot that matches the timestamp
* `evercam.snapshots.all(id, callback)` - Returns a list of all snapshots stored in a camera
* `evercam.snapshots.get(id, timestamp, [with_data], [range], callback)` - Get all snapshots in range of timestamp (default `range`: 0, `with_data`: true)
* `evercam.snapshots.range(id, timestamp, end_timestamp, [with_data], [page_limit], [page], callback)` - Get all snapshots in rnage, `with_data` is true by default

#### Logs

* `evercam.logs.get(id, [options], callback)` - Returns list of logs for a camera

#### Models

* `evercam.models.get([vendor], callback)` - Get all supported models from all vendors, or single vendor
* `evercam.models.camera(vendor, model, callback)` - Returns data for a particular camera model

#### Vendors

* `evercam.vendors.get([mac], callback)` - Returns all known IP hardware vendors, possibly filtered by MAC adress

#### User

* `evercam.user.create(options, callback)` - Create a new evercam account
* `evercam.user.cameras(user_id, [include_shared], callback)` - Returns the set of cameras associated with a user, `include_shared` defaults to false
* `evercam.user.update(user_id, options, callback)` - Update user account info
* `evercam.user.get(user_id, callback)` - Get user info, used by clients with a client API key
* `evercam.user.remove(user_id, callback)` - remove a user account

#### Shares

* `evercam.shares.get(camera_id, [user_id], callback)` - Get information of camera share to a ceratin user or all users
* `evercam.shares.create(camera_id, email, rights, [options], callback)` - Create a new camera share
* `evercam.shares.remove(camera_id, share_id, callback)` - remove a camera share
* `evercam.shares.change(share_id, rights, callback)` - Update rights for a camera share
* `evercam.shares.all(user_id, callback)` - Get all cameras shared with a user
* `evercam.shares.pending.get(camera_id, [status], callback)` - Get the list of pending share reuest for a camera
* `evercam.shares.pending.remove(camera_id, email, callback)` - Cancel a pending share reuqest to an email
* `evercam.shares.pending.change(share_id, rights, callback)` - Change rights on a pending share reuqest

#### Following

Comming soon...
It will be added once the end point is added.

### Known bugs/glitches

* Currently the '**GET** */cameras/test.json*', requires username & password, random ones are chosen if not explicitly set in the function call
* `evercam.snapshots.store()` is tested, but not fully, this is due to [Nock](//github.com/pgte/nockonsole.log) not liking binary data, see "Future plans" section for more

### Future plans

* Set up tests so that they can be run on the development server instead of mocking them

### More information

For more information see [offical docs](https://dashboard.evercam.io/dev)
