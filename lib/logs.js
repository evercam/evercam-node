var type = require('node-typed');


// Optional: extra(object)
exports.get = function(id, extra, callback) {
    if (arguments.length == 2) {
        callback = extra;
        extra = {};
    }
    id.type('id:string');
    extra.type('options:object');
    callback.type('callback:function');

    this.makeRequest('GET', '/cameras/'+id+'/logs.json', extra, callback);
};
