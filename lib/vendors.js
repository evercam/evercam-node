var type = require('node-typed');


// Optional: mac(string)
exports.get = function(mac, callback) {
    if (arguments.length == 1) {
        callback = mac;
        callback.type('callback:function');
        this.makeRequest('GET', '/vendors.json', {}, callback);
    } else {
        mac.type('mac:string');
        callback.type('callback:function');
        this.makeRequest('GET', '/vendors/'+mac+'.json', {}, callback);
    }
};
