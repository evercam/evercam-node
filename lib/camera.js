var type = require('node-typed');


// Tests if given camera parameters are correct
// FIXME: Right now there is a bug where cameras require a username & a password
//        Unitl fixed a random username & password are set to get around the probelm
exports.test = function(externalUrl, jpgUrl, username, password, callback) {
    if (arguments.length == 3) {
        callback = username;
        username = 'john';
        password = 'smith';
    } else if (arguments.length == 4) {
        throw new Error('Username, but not the password provided');
    }

    externalUrl.type('base_url:string');
    jpgUrl.type('jpg_url:string');
    username.type('username:string');
    password.type('password:string');
    callback.type('callback:function');
    var extra = {
        external_url: externalUrl,
        jpg_url: jpgUrl,
        cam_username: username,
        cam_password: password
    };
    this.makeRequest('GET', '/cameras/test.json', extra, callback);
};


// Returns all data for a specified set of cameras.
exports.get = function(ids, callback) {
    ids.type('ids_array:string,[string]');
    callback.type('callback:function');
    ids = this.ensureArray(ids).join(',');
    this.makeRequest('GET', '/cameras.json', {ids: ids}, callback);
};


// Updates full or partial data for an existing camera
exports.update = function(id, extra, callback) {
    id.type('id:string');
    extra.type('changes:object');
    callback.type('callback:function');
    this.makeRequest('PATCH', '/cameras/'+id+'.json', extra, callback);
};


// removes a camera from Evercam along with any stored media
exports.remove = function(id, callback) {
    id.type('id:string');
    callback.type('callback:function');
    this.makeRequest('DELETE', '/cameras/'+id+'.json', {}, callback);
};


// Transfers the ownership of a camera from one user to another
exports.transfer = function(id, userId, callback) {
    id.type('id:string');
    userId.type('user_id:string');
    callback.type('callback:function');
    this.makeRequest('PUT', '/cameras/'+id+'.json', {user_id: userId}, callback);
};


// Creates a new camera owned by the authenticating user
exports.create = function(extra, callback) {
    extra.type('options:object');
    callback.type('callback:function');
    this.makeRequest('POST', '/cameras.json', extra, callback);
};
