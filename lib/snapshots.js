var type = require('node-typed');


// Returns base64 encoded jpg from the camera as an object
exports.live = function(id, callback) {
    id.type('id:string');
    callback.type('callback:function');
    this.makeRequest('GET', '/cameras/'+id+'/live.json', {}, callback);
};


// Returns last snapshot data, `with_data` is true by default
// Optional: withData(boolean)
exports.last = function(id, withData, callback) {
    if (arguments.length == 2) {
        callback = withData;
        withData = true;
    }
    id.type('id:string');
    withData.type('with_data:boolean');
    callback.type('callback:function');

    var path = '/cameras/'+id+'/snapshots/latest.json';
    this.makeRequest('GET', path, {with_data: withData}, callback);
};


// Save the current camera snapshot, at the current timestamp
// Optional: note(string)
exports.save = function(id, note, callback) {
    if (arguments.length == 2) {
        callback = note;
        note = '';
    }
    id.type('id:string');
    note.type('note:string');
    callback.type('callback:function');

    var path = '/cameras/'+id+'/snapshots.json';
    this.makeRequest('POST', path, {notes: note}, callback);
};


// Upload an image, at a timestamp
// data needs to be a binary buffer
// Optional: note(string)
exports.store = function(id, timestamp, data, filename, note, callback) {
    if (arguments.length == 5) {
        callback = note;
        note = '';
    }
    id.type('id:string');
    timestamp.type('timestamp:date,number');
    data.type('data_buffer:buffer');
    filename.type('filename:string');
    note.type('note:string');
    callback.type('callback:function');
    timestamp = this.ensureTimestamp(timestamp);

    var path = '/cameras/'+id+'/snapshots/'+timestamp+'.json';
    this.makeRequest('UPLOAD', path, {data: data, filename: filename, notes: note}, callback);
};


// removes a snapshot that matches the timestamp
exports.remove = function(id, timestamp, callback) {
    id.type('id:string');
    timestamp.type('timestamp:date,number');
    callback.type('callback:function');
    timestamp = this.ensureTimestamp(timestamp);

    var path = '/cameras/'+id+'/snapshots/'+timestamp+'.json';
    this.makeRequest('DELETE', path, {}, callback);
};


// Returns a list of all snapshots stored in a camera
exports.all = function(id, callback) {
    id.type('id:string');
    callback.type('callback:function');
    this.makeRequest('GET', '/cameras/'+id+'/snapshots.json', {}, callback);
};


// Get all snapshots in range of timestamp (default `range`: 0, `with_data`: true)
// Optional: range(number), withData(boolean)
exports.get = function(id, timestamp, withData, range, callback) {
    if (arguments.length == 3) {
        callback = withData;
        withData = true;
        range = 1; // Appreantly 1 is the default range
    } else if (arguments.length == 4) {
        callback = range;
        range = 1;
    }
    id.type('id:string');
    timestamp.type('timestamp:date,number');
    withData.type('with_data:boolean');
    range.type('range:number');
    callback.type('callback:function');
    timestamp = this.ensureTimestamp(timestamp);

    var path = '/cameras/'+id+'/snapshots/'+timestamp+'.json';
    this.makeRequest('GET', path, {with_data: withData, range: range}, callback);
};


// Get all snapshots in rnage, `with_data` is true by default
// Optional: withData(boolean), limit(number), pageNo(number)
exports.range = function(id, fromTime, toTime, withData, limit, pageNo, callback) {
    if (arguments.length == 4) {
        callback = withData;
        withData = true;
        limit = 10; // Default value for with_data true
        pageNo = 0;
    } else if (arguments.length == 5) {
        callback = limit;
        limit = withData ? 10 : 100; // Default behaviour
        pageNo = 0;
    } else if (arguments.length == 6) {
        callback = pageNo;
        pageNo = 0;
    }
    id.type('id:string');
    fromTime.type('timestamp:date,number');
    toTime.type('end_timestamp:date,number');
    withData.type('with_data:boolean');
    limit.type('page_limit:number');
    pageNo.type('page:number');
    callback.type('callback:function');
    fromTime = this.ensureTimestamp(fromTime);
    toTime = this.ensureTimestamp(toTime);

    var extra = {
        from: fromTime,
        to: toTime,
        with_data: withData,
        limit: limit,
        page: pageNo
    };
    this.makeRequest('GET', '/cameras/'+id+'/snapshots/range.json', extra, callback);
};
