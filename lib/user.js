var type = require('node-typed');


// Create a new evercam account
exports.create = function(options, callback) {
    options.type('options:object');
    callback.type('callback:function');
    this.makeRequest('POST', '/users.json', options, callback);
};


// Returns the set of cameras associated with a user
// Optional: include_shared(boolean)
exports.cameras = function(userId, includeShared, callback) {
    if (arguments.length == 2) {
        callback = includeShared;
        includeShared = false;
    }
    userId.type('user_id:string');
    includeShared.type('include_shared:boolean');
    callback.type('callback:function');
    var path = '/users/'+userId+'/cameras.json';
    this.makeRequest('GET', path, {include_shared: includeShared}, callback);
};



// Update user account info
exports.update = function(userId, options, callback) {
    userId.type('user_id:string');
    options.type('options:object');
    callback.type('callback:function');
    this.makeRequest('PATCH', '/users/'+userId+'.json', options, callback);
};


// Get user info, used by clients with a client API key
exports.get = function(userId, callback) {
    userId.type('user_id:string');
    callback.type('callback:function');
    this.makeRequest('GET', '/users/'+userId+'.json', {}, callback);
};


// remove a user account
exports.remove = function(userId, callback) {
    userId.type('user_id:string');
    callback.type('callback:function');
    this.makeRequest('DELETE', '/users/'+userId+'.json', {}, callback);
};
