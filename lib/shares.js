var type = require('node-typed');

// Gosh! Pennding shares don't get along with normal shares at all
exports.pending = {};


// Get information of camera share to a ceratin user or all users
// Optional: userId(string)
exports.get = function(cameraId, userId, callback) {
    if (arguments.length == 2) {
        callback = userId;
        cameraId.type('camera_id:string');
        callback.type('callback:function');
        this.makeRequest('GET', '/shares/cameras/'+cameraId+'.json', {}, callback);
    } else {
        cameraId.type('camera_id:string');
        userId.type('user_id:string');
        callback.type('callback:function');
        this.makeRequest('GET', '/shares.json', {camera_id: cameraId, user_id: userId}, callback);
    }
};


// Create a new camera share
// Optional: options(object)
exports.create = function(cameraId, email, rights, options, callback) {
    if (arguments.length = 4) {
        callback = options;
        options = {};
    }
    cameraId.type('camera_id:string');
    email.type('email:string');
    rights.type('right:string');
    options.type('options:object');
    callback.type('callback:function');

    options.email = email;
    options.rights = rights;
    this.makeRequest('POST', '/shares/cameras/'+cameraId+'.json', options, callback);
};


// remove a camera share
exports.remove = function(cameraId, shareId, callback) {
    cameraId.type('camera_id:string');
    shareId.type('share_id:number');
    callback.type('callback:function');
    this.makeRequest('DELETE', '/shares/cameras/'+cameraId+'.json', {share_id: shareId}, callback);
};


// Update rights for a camera share
exports.change = function(shareId, rights, callback) {
    shareId.type('share_id:number');
    rights.type('rights:string');
    callback.type('callback:function');
    this.makeRequest('PATCH', '/shares/cameras/'+shareId+'.json', {rights: rights}, callback);
};


// Get all cameras shared with a user
exports.all = function(userId, callback) {
    userId.type('user_id:string');
    callback.type('callback:function');
    this.makeRequest('GET', '/shares/users/'+userId+'.json', {}, callback);
};


// Get the list of pending share reuest for a camera
// Optional: status(string)
// The status field doesn't appear to have any reasonable default value
exports.pending.get = function(cameraId, status, callback) {
    if (arguments.length == 2) {
        callback = status;
        var extra = {};
    } else {
        status.type('status:string');
        var extra = {status: status};
    }
    cameraId.type('camera_id:string');
    callback.type('callback:function');
    this.makeRequest('GET', '/shares/requests/'+cameraId+'.json', extra, callback);
};


// Cancel a pending share reuqest to an email
exports.pending.remove = function(cameraId, email, callback) {
    cameraId.type('camera_id:string');
    email.type('email:string');
    callback.type('callback:function');
    this.makeRequest('DELETE', '/shares/requests/'+cameraId+'.json', {email: email}, callback);
};


// Change rights on a pending share reuqest
exports.pending.change = function(shareId, rights, callback) {
    shareId.type('share_id:string');
    rights.type('rights:string');
    callback.type('callback:function');
    this.makeRequest('PATCH', '/shares/requests/'+shareId+'.json', {rights: rights}, callback);
};
