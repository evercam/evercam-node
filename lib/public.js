// Fetch a list of publicly discoverable cameras from within the Evercam system.
exports.cameras = function(extra, callback) {
    // Extra is optional
    if (callback == undefined) {
        callback = extra;
        extra = {};
    }

    this.makeRequest('GET', '/public/cameras.json', extra, callback);
}
