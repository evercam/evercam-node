var request = require('request');
var path = require('path');
var url = require('url');
var type = require('node-typed');


// Creates it's own function scope
function makeApplyFunc(func, data) {
    return function() {
        return func.apply(data, arguments);
    }
}


// Apply makeapplyfunc to all functions within an object or recurse down
function recursiveMap(obj, data) {
    for (var key in obj) {
        if (obj[key].type('function?')) {
            obj[key] = makeApplyFunc(obj[key], data);
        } else if (obj[key].type('object?')) {
            recursiveMap(obj[key], data);
        }
    }
}


function Evercam(apiId, apiKey) {
    apiKey.type('api_key:string');
    apiId.type('api_id:string');

    this.URL = 'https://api.evercam.io/v1';
    this.apiId = apiId;
    this.apiKey = apiKey;


    // Unfortunately if I assign this object to Evercam.prototype it would be
    // copied over when creating a new Evercam object this is bad for testing
    // and usability in general as it creates an implicit singleton. Those
    // objects have to be new each time since recursiveMap() wraps them in
    // another function that call()s them with the current this, greatly
    // simplifying their boiler plate code. It's a matter of sacrifice
    var evercam = {
        'public': require('./public'),
        'camera': require('./camera'),
        'snapshots': require('./snapshots'),
        'logs': require('./logs'),
        'vendors': require('./vendors'),
        'models': require('./models'),
        'user': require('./user'),
        'shares': require('./shares')
    };
    // Add module methods to this
    recursiveMap(evercam, this);
    for (var key in evercam) {
        this[key] = evercam[key];
    }
};


// Test if API keys are valid
Evercam.prototype.test = function(callback) {
    callback.type('callback:function');
    this.makeRequest('GET', '/test.json', {}, callback);
};


// Just a good method for mocking APIs
Evercam.prototype.url = function(url) {
    url.type('url:string');
    this.URL = url;
};


// Make a new request to Evercam endpoints
// UPLOAD is a special HTTP request method made up by me.
// It tells makeRequest() to use multipart/form-data instead of URL encoded form
// data to upload files. This was done to prevent code duplication
Evercam.prototype.makeRequest = function(method, apiUrl, extra, callback) {
    var method = method.toUpperCase();
    var extension = path.extname(url.parse(apiUrl).pathname);
    var params = {
        url: this.URL + apiUrl,
        method: method
    };

    if (extension == '.json') {
        params.json = true;
    } else {
        params.encoding = null;
    }

    if (this.apiId !== undefined && this.apiKey !== undefined) {
        extra.api_id = this.apiId;
        extra.api_key = this.apiKey;
    }

    switch (method) {
        case 'GET':
        case 'DELETE':
            params.qs = extra;
            break;
        case 'PUT':
        case 'PATCH':
        case 'POST':
            params.form = extra;
            break;
        case 'UPLOAD':
            params.method = 'POST'; // Override the default to set it to method
            break;
        default:
            throw new Error("Invalid HTTP request method");
    }

    var reqObject = request(params, function(err, res, body) {
        // It was suppsoed to be an object or a non x200/x300 status
        if (type('null?', body)
            || (!type('object,undefined?', body) && params.json) // DELETE returns undefined
            || res.statusCode < 200
            || res.statusCode >= 400) {
            callback(body, null);
        } else {
            callback(err, body);
        }
    });

    // I'm not sure how this works either.. I just took it from the docs and it works
    // Here be dragons: It turns out just uploading as buffer isn't enough, therafore
    // this code is now a speical case
    if (method == 'UPLOAD') {
        var form = reqObject.form();

        form.append('data', extra.data, {filename: extra.filename});
        delete extra.data;
        delete extra.filename;

        // Iterate over the rest of extra arguments (like API id & key, or notes)
        for (var key in extra) {
            if (extra[key] == '') continue; // Evercam doesn't like empty values
            form.append(key, extra[key]);
        }
    }
};


// Make an array if single value
Evercam.prototype.ensureArray = function(data) {
    if (!data.type('array?')) {
        return [data];
    }
    return data;
};


// Accepts date and number timestamp, always outputs just the number
Evercam.prototype.ensureTimestamp = function(date) {
    if (date.type('date?')) {
        return date.now();
    }
    return date;
}

module.exports = Evercam;
