var type = require('node-typed');


// Optional: vendor(string)
exports.get = function(vendor, callback) {
    if (arguments.length == 1) {
        callback = vendor;
        callback.type('callback:function');
        this.makeRequest('GET', '/models.json', {}, callback);
    } else {
        vendor.type('vendor:string');
        callback.type('callback:function');
        this.makeRequest('GET', '/models/'+vendor+'.json', {}, callback);
    }
};


exports.camera = function(vendor, model, callback) {
    vendor.type('vendor:string');
    model.type('mode:string');
    callback.type('callback:function');
    this.makeRequest('GET', '/models/'+vendor+'/'+model+'.json', {}, callback);
};
