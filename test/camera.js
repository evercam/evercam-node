var Evercam = require('../lib/evercam');
var url = require('./url');
var nock = require('nock');
var test = require('tape');


var evercam = new Evercam('1', '2');
evercam.url(url.URL);


test('Test if camera paramaters are correct', function(t) {
    var qs = 'external_url=example.org&' +
             'jpg_url=%2Fa.jpg&' +
             'cam_username=john&' +
             'cam_password=smith&' +
             'api_id=1&api_key=2';

    var scope = nock(url.BASE)
        .get('/v1/cameras/test.json?'+qs)
        .reply(200, {message: 'Camera offline'});

    t.plan(1);
    evercam.camera.test('example.org', '/a.jpg', function(err, res) {
        scope.done();
        // Many camera object props skipped
        t.equal(res.message, 'Camera offline');
    });
});


test('Throw an error if username but not the password are provided', function(t) {
    t.plan(1);
    t.throws(function() {
        evercam.camera.test('example.org', '/a.jpg', 'admin', new Function);
    }, 'Username, but not the password provided');
});


test('Get camera info', function(t) {
    var scope = nock(url.BASE)
        .get('/v1/cameras.json?ids=thatcam&api_id=1&api_key=2')
        .reply(200, {cameras: [{id: 'thatcam'}]})

    t.plan(1);
    evercam.camera.get('thatcam', function(err, res) {
        scope.done();
        // Many camera object props skipped
        t.equal(res.cameras[0].id, 'thatcam');
    });
});


test('Get multiple camera infos', function(t) {
    var scope = nock(url.BASE)
        .get('/v1/cameras.json?ids=thatcam%2Cthiscam&api_id=1&api_key=2')
        .reply(200, {cameras: [{id: 'thatcam'}, {id: 'thiscam'}]})

    t.plan(2);
    evercam.camera.get(['thatcam', 'thiscam'], function(err, res) {
        scope.done();
        // Many camera object props skipped
        t.equal(res.cameras[0].id, 'thatcam');
        t.equal(res.cameras[1].id, 'thiscam');
    });
});


test('Update camera info', function(t) {
    var scope = nock(url.BASE)
        .patch('/v1/cameras/thatcam.json', {is_public: 'true', api_id: '1', api_key: '2'})
        .reply(200, {cameras: [{id: 'thatcam'}]})

    t.plan(1);
    evercam.camera.update('thatcam', {'is_public': true}, function(err, res) {
        scope.done();
        // Many camera object props skipped
        t.equal(res.cameras[0].id, 'thatcam');
    });
});


test('remove a camera', function(t) {
    var scope = nock(url.BASE)
        .delete('/v1/cameras/thatcam.json?api_id=1&api_key=2')
        .reply(200);

    evercam.camera.remove('thatcam', function(err, res) {
        scope.done();
        t.end(); // Actaully returns nothing
    });
})


test('Transfer a camera', function(t) {
    var scope = nock(url.BASE)
        .put('/v1/cameras/thatcam.json', {user_id: 'thatuser', api_id: '1', api_key: '2'})
        .reply(200, {cameras: [{id: 'thatcam'}]})

    t.plan(1);
    evercam.camera.transfer('thatcam', 'thatuser', function(err, res) {
        scope.done();
        // Many camera object props skipped
        t.equal(res.cameras[0].id, 'thatcam');
    });
})


test('Create a camera', function(t) {
    var scope = nock(url.BASE)
        .post('/v1/cameras.json', {id: 'othercam', name: 'Other camera', is_public: 'true', api_id: '1', api_key: '2'})
        .reply(200, {cameras: [{id: 'thatcam'}]})

    t.plan(1);
    evercam.camera.create({id: 'othercam', name: 'Other camera', is_public: true}, function(err, res) {
        scope.done();
        // Many camera object props skipped
        t.equal(res.cameras[0].id, 'thatcam');
    });
})
