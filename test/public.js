var Evercam = require('../lib/evercam');
var url = require('./url');
var nock = require('nock');
var test = require('tape');


var evercam = new Evercam('1', '2');
evercam.url(url.URL);


test('Get all public cameras', function(t) {
    var scope = nock(url.BASE)
        .get('/v1/public/cameras.json?api_id=1&api_key=2')
        .reply(200, {cameras: [{id: 'thatcam'}], pages: 0});


    t.plan(2);
    evercam.public.cameras(function(err, res) {
        scope.done();
        t.equal(res.cameras[0].id, 'thatcam');
        t.equal(res.pages, 0);
    });
});


test('Limit public camera results', function(t) {
    var scope = nock(url.BASE)
        .get('/v1/public/cameras.json?id_starts_with=thatcam&api_id=1&api_key=2')
        .reply(200, {cameras: [{id: 'thatcam'}], pages: 0});

    t.plan(2);
    evercam.public.cameras({'id_starts_with': 'thatcam'}, function(err, res) {
        scope.done();
        t.equal(res.cameras[0].id, 'thatcam');
        t.equal(res.pages, 0);
    });
});
