var Evercam = require('../lib/evercam');
var url = require('./url');
var nock = require('nock');
var test = require('tape');


var evercam = new Evercam('1', '2');
evercam.url(url.URL);


test('Get live camera image', function(t) {
    var scope = nock(url.BASE)
        .get('/v1/cameras/thatcam/live.json?api_id=1&api_key=2')
        .reply(200, {camera: 'thatcam', data: 'base64'});

    t.plan(2);
    evercam.snapshots.live('thatcam', function(err, res) {
        scope.done();
        t.equal(res.camera, 'thatcam');
        t.equal(res.data, 'base64');
    });
});


test('Get latest snapshot image', function(t) {
    var scope = nock(url.BASE)
        .get('/v1/cameras/thatcam/snapshots/latest.json?with_data=true&api_id=1&api_key=2')
        .reply(200, {snapshots: [{camera: 'thatcam', notes: 'yey', data: 'base64'}]});

    t.plan(3);
    evercam.snapshots.last('thatcam', function(err, res) {
        scope.done();
        t.equal(res.snapshots[0].camera, 'thatcam');
        t.equal(res.snapshots[0].notes, 'yey');
        t.equal(res.snapshots[0].data, 'base64');
    });
});


test('Save current image as snapshot', function(t) {
    var scope = nock(url.BASE)
        .post('/v1/cameras/thatcam/snapshots.json', {notes: '', api_id: '1', api_key: '2'})
        .reply(200, {snapshots: [{camera: 'thatcam', notes: null}]});

    t.plan(2);
    evercam.snapshots.save('thatcam', function(err, res) {
        scope.done();
        t.equal(res.snapshots[0].camera, 'thatcam');
        t.equal(res.snapshots[0].notes, null);
    });
});


// TODO: Once I get around to use the test server instead of the mocked test,
// I'll test this method, until then everyone will just have to take my word for
// it, or try it themsellfs, I've got a script that checks it on my local machine,
// and I'll intergate it into the tests once I get more time
test('Store an image as a snapshot', function(t) {
    var scope = nock(url.BASE)
        .post('/v1/cameras/thatcam/snapshots/0.json') // Nock doesn't like binary :(
        .reply(201, {snapshots: [{camera: 'thatcam', notes: null, created_at: 0}]});

    // Base64 picture of a small red dot taken from en.wikipedia.org/wiki/Data_URI_scheme
    var buffer = new Buffer('iVBORw0KGgoAAAANSUhEUgAAAAUAAAAFCAYAAACNbyblAAAAHElEQVQI12P4//8/w38GIAXDIBKE0DHxgljNBAAO9TXL0Y4OHwAAAABJRU5ErkJggg==', 'base64');
    t.plan(3);
    evercam.snapshots.store('thatcam', 0, buffer, 'dot.png', function(err, res) {
        t.equal(res.snapshots[0].camera, 'thatcam');
        t.equal(res.snapshots[0].notes, null);
        t.equal(res.snapshots[0].created_at, 0);
    });
});


test('remove a snapshot', function(t) {
    var scope = nock(url.BASE)
        .delete('/v1/cameras/thatcam/snapshots/0.json?api_id=1&api_key=2')
        .reply(200);

    evercam.snapshots.remove('thatcam', 0, function(err, res) {
        t.end(); // Actaully returns nothing
        scope.done();
    });
});


test('Get all snapshots', function(t) {
    var scope = nock(url.BASE)
        .get('/v1/cameras/thatcam/snapshots.json?api_id=1&api_key=2')
        .reply(200, {snapshots: [{camera: 'thatcam', notes: 'yey'}]});

    t.plan(2);
    evercam.snapshots.all('thatcam', function(err, res) {
        t.equal(res.snapshots[0].camera, 'thatcam');
        t.equal(res.snapshots[0].notes, 'yey');
        scope.done();
    });
});


test('Get all snapshots in range of timestamp', function(t) {
    var scope = nock(url.BASE)
        .get('/v1/cameras/thatcam/snapshots/0.json?with_data=true&range=1&api_id=1&api_key=2')
        .reply(200, {snapshots: [{camera: 'thatcam', notes: 'yey'}]});

    t.plan(2);
    evercam.snapshots.get('thatcam', 0, function(err, res) {
        t.equal(res.snapshots[0].camera, 'thatcam');
        t.equal(res.snapshots[0].notes, 'yey');
        scope.done();
    });
});


test('Get all snapshots beetwen two timestamps', function(t) {
    var scope = nock(url.BASE)
        .get('/v1/cameras/thatcam/snapshots/range.json?from=0&to=1&with_data=true&limit=10&page=0&api_id=1&api_key=2')
        .reply(200, {snapshots: [{camera: 'thatcam', notes: 'yey'}]});

    t.plan(2);
    evercam.snapshots.range('thatcam', 0, 1, function(err, res) {
        t.equal(res.snapshots[0].camera, 'thatcam');
        t.equal(res.snapshots[0].notes, 'yey');
        scope.done();
    });
});
