var Evercam = require('../lib/evercam');
var url = require('./url');
var nock = require('nock');
var test = require('tape');


var evercam = new Evercam('1', '2');
evercam.url(url.URL);


test('Get all vendors', function(t) {
    var scope = nock(url.BASE)
        .get('/v1/vendors.json?api_id=1&api_key=2')
        .reply(200, {vendors: [{id: 'thatcam'}]})

    t.plan(1);
    evercam.vendors.get(function(err, res) {
        scope.done();
        // Many vendor object props skipped
        t.equal(res.vendors[0].id, 'thatcam');
    });
});


test('Filter vendors by MAC adress', function(t) {
    var scope = nock(url.BASE)
        .get('/v1/vendors/HE:LL:O.json?api_id=1&api_key=2')
        .reply(200, {vendors: [{id: 'thatcam'}]})

    t.plan(1);
    evercam.vendors.get('HE:LL:O', function(err, res) {
        scope.done();
        // Many vendor object props skipped
        t.equal(res.vendors[0].id, 'thatcam');
    });
});
