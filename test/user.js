var Evercam = require('../lib/evercam');
var url = require('./url');
var nock = require('nock');
var test = require('tape');


var evercam = new Evercam('1', '2');
evercam.url(url.URL);


test('Create a new user', function(t) {
    var scope = nock(url.BASE)
        .post('/v1/users.json', {username: 'thatguy', api_id: '1', api_key:'2'})
        .reply(200, {users: [{id: 'thatguy'}]});

    // Most user info omited on purpose
    t.plan(1);
    evercam.user.create({username: 'thatguy'}, function(err, res) {
        scope.done();
        t.equal(res.users[0].id, 'thatguy');
    });
});


test('Get user cameras', function(t) {
    var scope = nock(url.BASE)
        .get('/v1/users/thatguy/cameras.json?include_shared=false&api_id=1&api_key=2')
        .reply(200, {cameras: [{id: 'thatcam'}]});

    // Most user info omited on purpose
    t.plan(1);
    evercam.user.cameras('thatguy', function(err, res) {
        scope.done();
        t.equal(res.cameras[0].id, 'thatcam');
    });
});


test('Update user data', function(t) {
    var scope = nock(url.BASE)
        .patch('/v1/users/thatguy.json', {firstname: 'john', api_id: '1', api_key:'2'})
        .reply(200, {users: [{id: 'thatguy'}]});

    // Most user info omited on purpose
    t.plan(1);
    evercam.user.update('thatguy', {firstname: 'john'}, function(err, res) {
        scope.done();
        t.equal(res.users[0].id, 'thatguy');
    });
});


test('Get user data', function(t) {
    var scope = nock(url.BASE)
        .get('/v1/users/thatguy.json?api_id=1&api_key=2')
        .reply(200, {users: [{id: 'thatguy'}]});

    // Most user info omited on purpose
    t.plan(1);
    evercam.user.get('thatguy', function(err, res) {
        scope.done();
        t.equal(res.users[0].id, 'thatguy');
    });
});


test('remove a user', function(t) {
    var scope = nock(url.BASE)
        .delete('/v1/users/thatguy.json?api_id=1&api_key=2')
        .reply(200);

    evercam.user.remove('thatguy', function(err, res) {
        t.end(); // Actaully returns nothing
        scope.done();
    });
});
