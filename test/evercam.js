var Evercam = require('../lib/evercam');
var url = require('./url');
var nock = require('nock');
var test = require('tape');


var evercam = new Evercam('1', '2');
evercam.url(url.URL);


test('Check the provided API id & key', function(t) {
    var scope = nock(url.BASE)
        .get('/v1/test.json?api_id=1&api_key=2')
        .reply(200, {authenticated: true});

    t.plan(1);
    evercam.test(function(err, res) {
        scope.done();
        t.equal(res.authenticated, true);
    });
});


// Warning: This used an udncoumented and not-public function
test('2xx JSON requests that don\'t return JSON are errors', function(t) {
    var scope = nock(url.BASE)
        .get('/v1/woah.json?api_id=1&api_key=2')
        .reply(200, 'Not found')

    t.plan(2);
    evercam.makeRequest('GET', '/woah.json', {}, function(err, res) {
        t.equal(res, null);
        t.equal(err, 'Not found');
    });
});


// Warning: This used an udncoumented and not-public function
test('4xx & 5xx requests are always error, but a silent attempt is made to JSOn.parse them', function(t) {
    var scope = nock(url.BASE)
        .get('/v1/woah.json?api_id=1&api_key=2')
        .reply(418, 'I\'m a teapot');

    t.plan(2);
    evercam.makeRequest('GET', '/woah.json', {}, function(err, res) {
        t.equal(res, null);
        t.equal(err, 'I\'m a teapot');
    });
});
