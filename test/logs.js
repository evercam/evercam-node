var Evercam = require('../lib/evercam');
var url = require('./url');
var nock = require('nock');
var test = require('tape');


var evercam = new Evercam('1', '2');
evercam.url(url.URL);


test('Retrives logs', function(t) {
    var scope = nock(url.BASE)
        .get('/v1/cameras/thatcam/logs.json?api_id=1&api_key=2')
        .reply(200, {logs: ['stuff'], pages: 1});

    t.plan(2);
    evercam.logs.get('thatcam', function(err, res) {
        scope.done();
        t.equal(res.logs[0], 'stuff');
        t.equal(res.pages, 1);
    });
});
