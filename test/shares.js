var Evercam = require('../lib/evercam');
var url = require('./url');
var nock = require('nock');
var test = require('tape');


var evercam = new Evercam('1', '2');
evercam.url(url.URL);


test('Get all camera shares', function(t) {
    var scope = nock(url.BASE)
        .get('/v1/shares/cameras/thatcam.json?api_id=1&api_key=2')
        .reply(200, {shares: [{id: 42}]});

    t.plan(1);
    evercam.shares.get('thatcam', function(err, res) {
        scope.done();
        // Many share object props skipped
        t.equal(res.shares[0].id, 42);
    });
});


test('Get single user camera share', function(t) {
    var scope = nock(url.BASE)
        .get('/v1/shares.json?camera_id=thatcam&user_id=thatguy&api_id=1&api_key=2')
        .reply(200, {shares: [{id: 42}]});

    t.plan(1);
    evercam.shares.get('thatcam', 'thatguy', function(err, res) {
        scope.done();
        // Many share object props skipped
        t.equal(res.shares[0].id, 42);
    });
});


test('Share a camera with a user', function(t) {
    // Assumues user exists in the system, otheriwse share_request object is returned
    var scope = nock(url.BASE)
        .post('/v1/shares/cameras/thatcam.json', {email:'thatguy@email.com', rights: 'view', api_id: '1', api_key: '2'})
        .reply(200, {shares: [{id: 42}]});

    t.plan(1);
    evercam.shares.create('thatcam', 'thatguy@email.com', 'view', function(err, res) {
        scope.done();
        // Many share object props skipped
        t.equal(res.shares[0].id, 42);
    });
});


test('remove a non-pending share', function(t) {
    var scope = nock(url.BASE)
        .delete('/v1/shares/cameras/thatcam.json?share_id=42&api_id=1&api_key=2')
        .reply(200);

    evercam.shares.remove('thatcam', 42, function(err, res) {
        scope.done();
        t.end(); // Actaully returns nothing
    });
});


test('Update rights of non-pending share', function(t) {
    var scope = nock(url.BASE)
        .patch('/v1/shares/cameras/42.json', {rights: 'view,edit', api_id: '1', api_key: '2'})
        .reply(200, {shares: [{id: 42}]});

    t.plan(1);
    evercam.shares.change(42, 'view,edit', function(err, res) {
        scope.done();
        // Many share object props skipped
        t.equal(res.shares[0].id, 42);
    });
});


test('Get all cameras users shared with him/her/it', function(t) {
    var scope = nock(url.BASE)
        .get('/v1/shares/users/thatguy.json?api_id=1&api_key=2')
        .reply(200, {shares: [{id: 42}]});

    t.plan(1);
    evercam.shares.all('thatguy', function(err, res) {
        scope.done();
        // Many share object props skipped
        t.equal(res.shares[0].id, 42);
    });
});


test('Get all pending requests', function(t) {
    var scope = nock(url.BASE)
        .get('/v1/shares/requests/thatcam.json?api_id=1&api_key=2')
        .reply(200, {share_request: [{id: 'deadbeef'}]});

    t.plan(1);
    evercam.shares.pending.get('thatcam', function(err, res) {
        scope.done();
        // Many share object props skipped
        t.equal(res.share_request[0].id, 'deadbeef');
    });
});


test('Abord a pending request', function(t) {
    var scope = nock(url.BASE)
        .delete('/v1/shares/requests/thatcam.json?email=thatguy%40email.com&api_id=1&api_key=2')
        .reply(200);

    evercam.shares.pending.remove('thatcam', 'thatguy@email.com', function(err, res) {
        scope.done();
        t.end(); // Actaully returns nothing
    });
});


test('Change rights of a pending request', function(t) {
    var scope = nock(url.BASE)
        .patch('/v1/shares/requests/deadbeef.json', {rights: 'view', api_id: '1', api_key: '2'})
        .reply(200, {share_request: [{id: 'deadbeef'}]});

    t.plan(1);
    evercam.shares.pending.change('deadbeef', 'view', function(err, res) {
        scope.done();
        // Many share object props skipped
        t.equal(res.share_request[0].id, 'deadbeef');
    });
});
