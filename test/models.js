var Evercam = require('../lib/evercam');
var url = require('./url');
var nock = require('nock');
var test = require('tape');

var evercam = new Evercam('1', '2');
evercam.url(url.URL);


test('Get all models from vendors', function(t) {
    var scope = nock(url.BASE)
        .get('/v1/models.json?api_id=1&api_key=2')
        .reply(200, {vendors: [{id: 'thatcam'}]})

    t.plan(1);
    evercam.models.get(function(err, res) {
        scope.done();
        // Many model object props skipped
        t.equal(res.vendors[0].id, 'thatcam');
    });
});


test('Get single model info', function(t) {
    var scope = nock(url.BASE)
        .get('/v1/models/thatcam/v2.json?api_id=1&api_key=2')
        .reply(200, {models: [{vendor: 'thatcam'}]})

    t.plan(1);
    evercam.models.camera('thatcam', 'v2', function(err, res) {
        scope.done();
        // Many model object props skipped
        t.equal(res.models[0].vendor, 'thatcam');
    });
});
